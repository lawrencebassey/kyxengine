package com.company.map;

import com.company.map.enums.*;
import java.util.ArrayList;
import java.util.List;

//Rename to something more sensible
public class KybCheckCalculator {

    public List<KyxCheck> getChecks(KYBLevel kybLevel) {
        List<KyxCheck> checks = new ArrayList<>();
        // For kycLevel B we need to do:
        if (kybLevel == KYBLevel.KYB_B) {
            checks.add(new PepSanctionsKyxCheck(EntityType.APPLICANT, CheckBand.KYB_BAND));
            checks.add(new BlocklistKYXCheck(EntityType.APPLICANT, CheckBand.KYB_BAND));
            checks.add(new GenericKYXCheck(CheckType.ID_SCAN, EntityType.INDIVIDUAL_DIRECTOR, CheckBand.KYC_BAND));
        }
        return checks;
    }
}
