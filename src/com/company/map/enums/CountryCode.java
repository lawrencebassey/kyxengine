package com.company.map.enums;

public enum CountryCode {
    GB("United Kingdom"),
    IN("India");

    private String name;

    CountryCode(String name) {
        this.name = name;
    }

    public String getFullName() {
        return this.name;
    }
}
