package com.company.map.enums;

public enum KYCLevel {
    KYC_A,
    KYC_B,
    KYC_C
}
