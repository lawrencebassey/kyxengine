package com.company.map.enums;

public enum EntityType {
    APPLICANT,
    INDIVIDUAL_DIRECTOR,
    INDIVIDUAL_SHAREHOLDER,
    BUSINESS,
    BUSINESS_DIRECTOR,
    BUSINESS_SHAREHOLDER,
}
