package com.company.map.enums;

public enum CheckType {
    BLOCKLIST,
    PEPS_SANCTIONS,
    ID_SCAN,
    SANCTIONS,
    IDENTITY
}
