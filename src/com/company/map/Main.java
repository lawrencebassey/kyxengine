package com.company.map;


import com.company.map.enums.ApplicationType;
import java.util.UUID;

import static java.lang.String.format;

public class Main {

    public static void main(String[] args) {

        ChecksService checkService = new ChecksService();
        Application application = Application.builder()
                .id(UUID.randomUUID())
                .applicationType(ApplicationType.UK_OPENACCESS)
                .build();

        boolean checksComplete = checkService.areChecksComplete(application);
        System.out.println(format("Checks complete for [%s] complete = [%s]", application.getId(), checksComplete));

    }
}
