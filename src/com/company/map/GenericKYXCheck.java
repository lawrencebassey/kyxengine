package com.company.map;

import com.company.map.enums.CheckBand;
import com.company.map.enums.CheckType;
import com.company.map.enums.EntityType;

import static java.lang.String.format;

public class GenericKYXCheck implements KyxCheck {

    private final CheckType checkType;
    private final EntityType entityType;
    private final CheckBand checkBand;

    public GenericKYXCheck(CheckType checkType, EntityType entityType, CheckBand checkBand) {
        this.checkType = checkType;
        this.entityType = entityType;
        this.checkBand = checkBand;
    }

    @Override
    public boolean isCheckComplete(Application application) {

        System.out.println(format("[%s] Kyx Check [%s] at [%s] for [%s] in Country [%s]- checking the DB!!",
                checkType, application.getId(), checkBand, entityType, application.getApplicationType().getContryCode().getFullName()));

        return false;
    }
}
