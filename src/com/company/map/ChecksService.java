package com.company.map;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChecksService {

    //Are all the checks for this application complete?
    public boolean areChecksComplete(Application application) {
        Map<KyxCheck, Boolean> checksMap = new HashMap<>();
        checksMap = findCheckStatus(application);
        return computeChecksStatus(checksMap);
    }

    private Map<KyxCheck, Boolean> findCheckStatus(Application application) {
        Map<KyxCheck, Boolean> checksMap = new HashMap<>();

        //KYc
        List<KyxCheck> kycChecks = new KycCheckCalculator().getChecks(application.getApplicationType().getKycLevel());
        for (KyxCheck kycCheck : kycChecks) {
            boolean kycCheckComplete = kycCheck.isCheckComplete(application);
            checksMap.put(kycCheck, kycCheckComplete);
        }

        //KYB
        List<KyxCheck> kybChecks = new KybCheckCalculator().getChecks(application.getApplicationType().getKybLevel());
        for (KyxCheck kybCheck : kybChecks) {
            boolean kybCheckComplete = kybCheck.isCheckComplete(application);
            checksMap.put(kybCheck, kybCheckComplete);
        }

        return checksMap;
    }

    //Returns true if there's nothing in the check map
    private boolean computeChecksStatus(Map<KyxCheck, Boolean> checksMap) {
        return !checksMap.containsValue(false);
    }

}
