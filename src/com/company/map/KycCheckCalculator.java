package com.company.map;

import com.company.map.enums.CheckBand;
import com.company.map.enums.EntityType;
import com.company.map.enums.KYCLevel;
import java.util.ArrayList;
import java.util.List;

//rename to something more sensible
public class KycCheckCalculator {

    public List<KyxCheck> getChecks(KYCLevel kycLevel) {
        List<KyxCheck> checks = new ArrayList<>();
        // For kycLevel B we need to do:
        if (kycLevel == KYCLevel.KYC_B) {
            checks.add(new PepSanctionsKyxCheck(EntityType.INDIVIDUAL_DIRECTOR, CheckBand.KYC_BAND));
            checks.add(new BlocklistKYXCheck(EntityType.INDIVIDUAL_DIRECTOR, CheckBand.KYC_BAND));
        }
        return checks;
    }
}
