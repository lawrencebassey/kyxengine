package com.company.map;

import com.company.map.enums.CheckType;
import com.company.map.enums.EntityType;

public interface KyxCheck {

    //returns true if check is complete at db
    boolean isCheckComplete(Application application);
}
