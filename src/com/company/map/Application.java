package com.company.map;

import com.company.map.enums.ApplicationType;
import java.util.UUID;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Application {

    UUID id;
    ApplicationType applicationType;

}
