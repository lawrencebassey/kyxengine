package com.company.map;

import com.company.map.enums.CheckBand;
import com.company.map.enums.CheckType;
import com.company.map.enums.EntityType;
import lombok.AllArgsConstructor;

import static java.lang.String.format;

@AllArgsConstructor
public class PepSanctionsKyxCheck implements KyxCheck {

    private final String CHECK_NAME = "PEPSANDSANCTIONS";

    private final EntityType entityType;
    private final CheckBand checkBand;

    @Override
    public boolean isCheckComplete(Application application) {
        System.out.println(format(" [%s] Kyx Check [%s] at [%s] for [%s]- checking the DB!!",
                CHECK_NAME, application.getId(), checkBand, entityType));
        //SQL
        //select status from "check" where application_id = '1'
        //and entity_type = 'APPLICANT'
        //and check_type = 'PEPSANDSANCTIONS'
        return false;
    }
}
