package com.company.map.enums;

import static com.company.map.enums.CountryCode.GB;
import static com.company.map.enums.CountryCode.IN;

public enum ApplicationType {
    UK_OPENACCESS(KYBLevel.KYB_B, KYCLevel.KYC_B, GB),
    IN_SAVINGS(KYBLevel.KYB_B, KYCLevel.KYC_A, IN);

    private final KYBLevel kybLevel;
    private final KYCLevel kycLevel;
    private final CountryCode countryCode;


    ApplicationType(KYBLevel kyb, KYCLevel kyc, CountryCode cc) {
        this.kybLevel = kyb;
        this.kycLevel = kyc;
        this.countryCode = cc;
    }

    public KYBLevel getKybLevel() {
        return kybLevel;
    }
    public KYCLevel getKycLevel() {
        return kycLevel;
    }
    public CountryCode getContryCode() { return countryCode; }
}
